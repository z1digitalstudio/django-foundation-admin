(function($){
  $('[sticky]').each(function(){
    var iElm = $(this);
    var cElm = iElm.clone();
    var keepWidthSelector = iElm.attr('sticky-keep-width');
    var isSticky = false;
    var parentElm = iElm.parent();
    cElm.removeAttr('sticky');
    cElm.find(':input').prop('disabled', true);
    cElm.css('display', 'none');
    iElm.after(cElm);

    var smWidth = 47.9375;
    
    $(window).on("resize", computeScroll);
    $(window).on("scroll", computeScroll);

    computeScroll();
    function computeScroll() {
      var currentWidth = $(window).width();
      var currentWidthEms = currentWidth / parseFloat($("body").css("font-size"))
      var isNotSmall = (currentWidth >= smWidth);
      doApplySticky(isNotSmall, isTopScrolled(), isBottomScrolled());
    }
    
    function getDocHeight() {
        return Math.max(
            document.body.scrollHeight, document.documentElement.scrollHeight,
            document.body.offsetHeight, document.documentElement.offsetHeight,
            document.body.clientHeight, document.documentElement.clientHeight
        );
    }

    function getAbsolutePos(el) {
        // yay readability
        for (var lx=0, ly=0;
             el != null;
             lx += el.offsetLeft, ly += el.offsetTop, el = el.offsetParent);
        return {left: lx, top: ly};
    }

    function getTopOffset(){
      return parseInt(iElm.attr('sticky-offset') || 0);
    }

    function isTopScrolled(){
      return getTopScrollPosition() < window.scrollY;
    }

    function isBottomScrolled(){
      var endOffset = parseInt(iElm.attr('sticky-end-offset') || 0);
      // We take the position of the cloned element.
      var endTopPosition = window.scrollY + iElm.outerHeight() + getTopOffset();
      var endPosition = endTopPosition + endOffset;

      var bottomPosition = getAbsolutePos(parentElm.get(0)).top + parentElm.outerHeight();
      return endPosition > bottomPosition;
    }

    function getTopScrollPosition(){
      var startOffset = parseInt(iElm.attr('sticky-start-offset') || 0);
      // We take the position of the cloned element.
      var topPosition = isSticky? cElm.position().top: iElm.position().top;
      return topPosition - startOffset;
    }

    function doApplySticky(isNotSmall, isTopScrolled, isBottomScrolled){
      isSticky = isNotSmall && isTopScrolled && !isBottomScrolled;
      if (isNotSmall && isTopScrolled){
        if (isBottomScrolled) {
          applyStickyAbsolute();
        } else {
          applyStickyFixed();
        }
      }else{
        unapplySticky();
      }
    }
    function applyStickyFixed(){
      parentElm.css('position', '');

      cElm.css('display', '');
      cElm.css('visibility', 'hidden');

      iElm.css('position', 'fixed');
      iElm.css('width', cElm.outerWidth());
      
      if (!!keepWidthSelector){
        var cloneElms = cElm.find(keepWidthSelector);
        var initElms = iElm.find(keepWidthSelector);
        for (var i = initElms.length - 1; i >= 0; i--) {
          var initElm = $(initElms[i]);
          var cloneElm = $(cloneElms[i]);
          initElm.css('width', cloneElm.outerWidth());
        }
      }

      iElm.css('top', getTopOffset() + 'px');
      iElm.css('left', cElm.position().left);
      iElm.css('bottom', '');
      iElm.css('right', '');
    }
    function applyStickyAbsolute(){
      parentElm.css('position', 'relative');
      iElm.css('position', 'absolute');
      iElm.css('width', '');
      iElm.css('top', '');
      iElm.css('left', '');
      iElm.css('bottom', 0);
      iElm.css('right', 0);

      cElm.css('display', '');
      cElm.css('visibility', 'hidden');
    }
    function unapplySticky(){
      // Clean styles when unapplying 
      parentElm.css('position', '');

      iElm.css('position', '');
      iElm.css('width', '');
      iElm.css('top', '');
      iElm.css('left', '');
      iElm.css('bottom', '');
      iElm.css('right', '');

      cElm.css('display', 'none');
    }
  });
})(jQuery || django.jQuery);





