from django.apps import AppConfig


class FoundationAdminConfig(AppConfig):
    name = 'foundation_admin'
