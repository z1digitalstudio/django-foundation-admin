# -*- coding: utf-8 -*-
from django import template

register = template.Library()


@register.filter
def pdb(element):
    import ipdb; ipdb.set_trace()
    return element


@register.simple_tag(takes_context=True)
def pdb(context, *args, **kwargs):
    import ipdb; ipdb.set_trace()
    return ''


@register.tag(name='capture_as')
def do_capture_as(parser, token):
    try:
        tag_name, args = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError("'capture_as' node requires a variable name.")
    nodelist = parser.parse(('endcapture_as',))
    parser.delete_first_token()
    return CaptureAsNode(nodelist, args)


class CaptureAsNode(template.Node):
    def __init__(self, nodelist, varname):
        self.nodelist = nodelist
        self.varname = varname

    def render(self, context):
        output = self.nodelist.render(context)
        context[self.varname] = output
        return ''


@register.tag(name='capture_as_req')
def do_capture_as_req(parser, token):
    try:
        tag_name, args = token.contents.split(None, 1)
    except ValueError:
        raise template.TemplateSyntaxError("'capture_as_req' node requires a variable name.")
    nodelist = parser.parse(('endcapture_as',))
    parser.delete_first_token()
    return CaptureAsNodeReq(nodelist, args)


class CaptureAsNodeReq(template.Node):
    def __init__(self, nodelist, varname):
        self.nodelist = nodelist
        self.varname = varname

    def render(self, context):
        output = self.nodelist.render(context)
        context[self.varname] = output
        setattr(context['request'], self.varname, output)
        return ''
