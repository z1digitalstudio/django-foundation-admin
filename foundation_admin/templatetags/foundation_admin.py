from django.template import Library

from django.utils.text import slugify
from django.template.loader import get_template
from django.utils.safestring import mark_safe

register = Library()

"""

sticky_navbar = True|False

top_menu = True|False
left_menu = True|False

actions_on_sidebar = True|False
actions_on_bottom = True|False

extra_html =
    {
        'add|show|change|changelist|admin':{
            'title|results|filters|pagination|fieldsetname|sidebar|actions': {
                'before|after': 'path-to-template.html',
            },
            ...
        },
        ...
    }

"""


def get_model_admin(request):
    model_admin = None

    view_func = request.resolver_match.func
    if hasattr(view_func, 'model_admin'):
        model_admin = view_func.model_admin
    elif hasattr(view_func, 'admin_site'):
        model_admin = view_func.admin_site

    return model_admin


@register.filter
def model_admin_attr(request, attr_name, default=None):
    model_admin = get_model_admin(request)
    if model_admin is None:
        return default
    return getattr(model_admin, attr_name, default)


@register.filter
def use_sticky_navbar(request):
    return model_admin_attr(request, 'sticky_navbar', True)


@register.filter
def use_top_menu(request):
    return model_admin_attr(request, 'top_menu', True)


@register.filter
def use_left_menu(request):
    return model_admin_attr(request, 'left_menu', False)


@register.filter
def use_actions_on_sidebar(request):
    return model_admin_attr(request, 'actions_on_sidebar', True)


@register.filter
def use_actions_on_bottom(request):
    return model_admin_attr(request, 'actions_on_bottom', False)


@register.filter
def admin_media(request):
    return model_admin_attr(request, 'media', '')


@register.filter
def admin_help_text(request):
    return model_admin_attr(request, 'help_text', '')


@register.simple_tag(takes_context=True)
def admin_extra_html(context, model_admin, view_name, block, position):
    """
        This property allows the user to include extra snippets of html code
        in the change_form view. The given value must be a dictionary of the
        type:
            {
                'block': {
                    'before|after': 'path-to-template.html',
                }
            }

        All views have 'title' location.

        For show / change views, is also possible to include html in the sidebar block.

        For changelists, is possible to include html in:
            - filters
            - results
            - pagination

        With view name 'admin', it will be used as a fallback.
    """
    if not model_admin:
        model_admin = get_model_admin(context['request'])

    extra_html = getattr(model_admin, 'extra_html', {})
    fallback_extra_html = extra_html.get('admin', {})

    extra_view_html = extra_html.get(view_name, fallback_extra_html)

    template_html = None
    for name, extra in extra_view_html.iteritems():
        if slugify(name) == slugify(block):
            for template_position, template_path in extra.iteritems():
                if position == template_position:
                    template_html = get_template(template_path)
                    break

    # It was not found on the extra_view_html, let's try to find it on fallback
    if template_html is None:
        for name, extra in fallback_extra_html.iteritems():
            if slugify(name) == slugify(block):
                for template_position, template_path in extra.iteritems():
                    if position == template_position:
                        template_html = get_template(template_path)
                        break

    if template_html is not None:
        return mark_safe(template_html.render(context))

    return ''
