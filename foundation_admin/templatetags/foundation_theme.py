from django.template import Library
from django.conf import settings
register = Library()


@register.inclusion_tag('admin/templatetags/themes/theme.css.html')
def foundation_theme(variables={}, theme="default"):

    # TODO Fetch from settings
    defaults = {
        'dark_blue': '#417690',
        'light_blue': '#79aec8',
        'lighter_blue': '#c4dce8',
        'light_beige': '#FFC',
        'light_grey': '#e6e6e6',
        'link_color': '#447e9b',
        'link_color_dark': '#447e9b',
        'link_color_hover': '#036',
        'link_color_light': '#5b80b2',

        # Navbar
        'navbar_color': "#FFC",
        'navbar_color_hover': '#FF6',
        'navbar_bg_color': '#417690',
        'navbar_label_color': "#b4bcc2",
        'navbar_label_bg_color': "#FFFFFF",
        'navbar_item_color': "#7b8a8b",
        'navbar_item_color_hover': "#FFFFFF",
        'navbar_item_bg_color': "#FFFFFF",
        'navbar_item_bg_color_hover': "#417690",
    }
    if hasattr(settings, 'FOUNDATION_ADMIN_THEMES'):
        themes = settings.FOUNDATION_ADMIN_THEMES
        sel_theme = themes[theme]

        defaults.update(sel_theme)

    # Given variables goes last
    defaults.update(variables)

    return defaults
